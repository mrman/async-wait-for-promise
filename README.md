# `async-wait-for-promise`

Re-run a given asynchronous function (any function which returns a `Promise`), waiting for that function's result to be non-`null` within a bounded timeframe (timeout).

This project assumes you already have either platform support for promises (Node 0.12+) or you have a polyfill like [es6-promise](https://www.npmjs.com/package/es6-promise).

## Installation

```console
$ npm install async-wait-for-promise
```

## Arguments

| Argument          | Type                       | Description                                                  |
|-------------------|----------------------------|--------------------------------------------------------------|
| `fn`              | `() => Promise<T \| null>` | async function to run as a check (returns a value or `null`) |
| `opts.intervalMs` | `number`                   | How often the function should be checked, in milliseconds    |
| `opts.timeoutMs`  | `number`                   | Timeout in milliseconds                                      |

## Usage

Provide a function (`fn`) that produces a value or `null`. Checking will continue until the function provides a non-null value (and return that value).

## Example

Given `db.query('<some identifier>')` function that returns:

- the object that you're looking for
- `null` when the object is missing

Here is how you'd use `async-wait-for-promise` to wait for completion:

```typescript
  const retrievedObject = await waitFor(() => db.query('object-id'));
```

A slightly more complete solution with error handling:

```typescript
import { waitFor, TimeoutError } from "async-wait-for-promise"

// Try to find an object that is being created somewhere else,
// once the object is present, it will be returned as the result
// (default timeout is 10 seconds)
try {
  const retrievedObject = await waitFor(() => db.query('object-id'));
  console.log("retrieved object:", retrievedObject);
} catch (err) {
    if (err instanceof TimeoutError) {
      // Handle timeout error
    }

    // Handle unexpected error
}
```

**NOTE** The function you pass to `waitFor` *must* return `null` when it is "not ready", and some non-`null` value otherwise. `false` is a non-null value, and will cause `waitFor` to finish.

## ES2015 (async/await)

```typescript
import { waitFor } from "async-wait-for-promise";

// Initial state
let a = 1;

// Add 1 every second
const interval = setInterval(() => a++, 1000);

// Wait for a to equal 5
const result = await waitFor(
  // remember, we return null to indicate to keep checking
  async () => a >= 5 || null,
  { timeoutMs: 10 * 1000 },
);
// At this point, result === true

clearInterval(interval);
```

## ES6

```javascript
"use strict";

const { waitFor, TimeoutError } = require("async-wait-for-promise");

waitFor(() => functionThatReturnsNullUntilTheRealValue())
  .then(result => console.log("result was:", result))
  .catch(err => {
    if (err instanceof TimeoutError) {
      // Handle timeout error (by default 10 seconds)
    }

    // Handle some unexpected error
  })
```

## FAQ

### Why use `null` as an indicator for termination?

[Using `null` is generally a mistake](https://www.infoq.com/presentations/Null-References-The-Billion-Dollar-Mistake-Tony-Hoare/) in the presences of better types, but this library uses `null` to indicate a very specific state -- the computation you passed resolving to a value that *is not satisfactory*. `null` is nice for this purpose as opposed to a general "falsy" check as `false` could very well be a value that the function *should* return, where `null` is less likely to be (how often do you write functions that are *supposed* to return `null`?).
