.PHONY: all \
				check-tool-pnpm check-tool-entr \
				dist install clean \
				changelog \
				build build-watch \
				lint lint-watch \
				test test-unit \
				target-dir publish publish-prerelease \
				publish-major publish-minor publish-patch \
				publish-preminor publish-premajor publish-prepatch

all: install build

PNPM ?= pnpm
NPM ?= npm
ENTR ?= entr
TAPE ?= ./node_modules/.bin/tape
GIT ?= git

PACKAGE_NAME ?= async-wait-for-promise
VERSION ?= $(shell grep version package.json | cut -d ' ' -f 4 | tr -d ,\")

check-tool-entr:
	@which entr > /dev/null || (echo -e "\n[ERROR] please install entr (https://entrproject.org/)" && exit 1)

check-tool-pnpm:
	@which pnpm > /dev/null || (echo -e "\n[ERROR] please install pnpm (https://pnpm.io/)" && exit 1)

install: dist
	@echo -e "=> running pnpm install..."
	$(PNPM) install

###############
# Development #
###############

print-version:
	@echo -n "$(VERSION)"

CHANGELOG_FILE_PATH ?= CHANGELOG

changelog:
	$(GIT) cliff --unreleased --tag=$(VERSION) --prepend=$(CHANGELOG_FILE_PATH)

lint:
	$(PNPM) lint

lint-watch: check-tool-entr
	find . -name "*.ts" | $(ENTR) -rc $(PNPM) lint

build: dist
	$(PNPM) build

build-watch: dist
	$(PNPM) build-watch

clean:
	rm -rf dist/*

#########
# Tests #
#########

test: test-unit

test-unit: check-tool-pnpm
	$(PNPM) test-unit

#############
# Packaging #
#############

PACKAGE_FILENAME ?= $(PACKAGE_NAME)-$(VERSION).tgz
TARGET_DIR ?= target
PACKAGE_PATH ?= $(TARGET_DIR)/$(PACKAGE_FILENAME)

target-dir:
	mkdir -p $(TARGET_DIR)

print-package-filename:
	@echo "$(PACKAGE_FILENAME)"

# NOTE: if you try to test this package locally (ex. using `pnpm add path/to/async-wait-for-promise-<version>.tgz`),
# You may have to  clean caches on every update.
package: clean build target-dir
	$(PNPM) pack
	mv $(PACKAGE_FILENAME) $(TARGET_DIR)/

# ex. patch | minor | patch | prerelease
PUBLISH_LEVEL ?= "minor"

PUBLISH_TAG ?= ""
ifeq ($(PUBLISH_TAG),"")
	PUBLISH_TAG_ARG = ""
else
	PUBLISH_TAG_ARG = "--tag=$(PUBLISH_TAG)"
endif

publish:
	$(PNPM) version $(PUBLISH_LEVEL) --no-git-tag-version
	$(MAKE) changelog
	$(GIT) commit -am "release: v$$($(MAKE) print-version)"
	$(MAKE) --quiet --no-print-directory package
	$(PNPM) publish $(PUBLISH_TAG_ARG)

publish-major:
	$(MAKE) --quiet --no-print-directory publish PUBLISH_LEVEL=minor PUBLISH_TAG=latest

publish-minor:
	$(MAKE) --quiet --no-print-directory publish PUBLISH_LEVEL=minor PUBLISH_TAG=latest

publish-patch:
	$(MAKE) --quiet --no-print-directory publish PUBLISH_LEVEL=patch PUBLISH_TAG=latest

publish-preminor:
	$(MAKE) --quiet --no-print-directory publish PUBLISH_LEVEL=preminor PUBLISH_TAG=pre

publish-premajor:
	$(MAKE) --quiet --no-print-directory publish PUBLISH_LEVEL=premajor PUBLISH_TAG=pre

publish-prepatch:
	$(MAKE) --quiet --no-print-directory publish PUBLISH_LEVEL=prepatch PUBLISH_TAG=pre
