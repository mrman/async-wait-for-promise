# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.1] - 2021-05-21

### Changed

- Update README

## [1.0.0] - 2021-03-13

### Changed

- Version (`0.1.0` -> `1.0.0`)
- README changes
- CI configuration fixes

## [0.1.0] - 2021-03-13

### Added
-  Implementation, tests

[Unreleased]: https://gitlab.com/mrman/async-wait-for-promise/compare/v1.0.1...HEAD
[1.0.1]: https://gitlab.com/mrman/async-wait-for-promise/releases/tag/v0.1.0...v1.0.1
[1.0.0]: https://gitlab.com/mrman/async-wait-for-promise/releases/tag/v0.1.0...v1.0.0
[0.1.0]: https://gitlab.com/mrman/async-wait-for-promise/releases/tag/v0.1.0
