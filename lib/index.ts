export class TimeoutError extends Error {
  public conditionName?: string;

  constructor(conditionName?: string) {
    let msg = "Timed out while waiting for condition";
    if (conditionName) {
      msg += ` named condition [${conditionName}]`;
    }
    super(msg);
    this.name = "TimeoutError";
    this.conditionName = conditionName;
  }
}

// Error that is thrown when an invalid function is used
export class InvalidFunctionError extends Error {
  constructor(m?: string) {
    super(m || "Missing/Invalid function");
  }
}

interface WaitForOpts {
  intervalMs?: number;
  timeoutMs?: number;
  name?: string;
}

/**
 * Wait until a given function produces a value
 *
 * @param {object} opts
 * @param {Function} opts.fn - the function used ot control checking, waiting until true is returned
 * @param {number} opts.intervalMs - the amount of time to wait in milliseconds
 * @param {number} opts.timeoutMs - the total amount of time to wait before issuing a TimeoutError
 * @returns {Promise<void>}
 */
export async function waitFor<T>(
  fn: () => Promise<T | null>,
  opts?: WaitForOpts,
): Promise<T> {
  const intervalMs = opts?.intervalMs ?? 500;
  const timeoutMs = opts?.timeoutMs ?? 10000;

  // Trust but verify
  if (intervalMs < 0) {
    throw new TypeError("Invalid intervalMs, cannot be less than 0");
  }
  if (timeoutMs < 0) {
    throw new TypeError("Invalid timeoutMs, cannot be less than 0");
  }

  return new Promise<T>((resolve, reject) => {
    // Check every second for function to evaluate to true
    const startTime = new Date().getTime();
    const interval = setInterval(async () => {
      // Ensure opts are still properly formed
      if (!fn || typeof fn !== "function") {
        clearInterval(interval);
        reject(new InvalidFunctionError());
        return;
      }

      // If we've waited too long then clear interval and exit
      const elapsedMs = new Date().getTime() - startTime;
      if (elapsedMs >= timeoutMs) {
        clearInterval(interval);
        reject(new TimeoutError(opts?.name));
        return;
      }

      try {
        const result = await fn();

        // If the result is null we want to not resolve *or* reject
        // and keep checking
        if (result === null) {
          return;
        }

        // Return any non-null results
        clearInterval(interval);
        resolve(result);
      } catch (err) {
        // If the function throws an error we'll bubble it up
        clearInterval(interval);
        reject(err);
      }
    }, intervalMs);
  });
}

export default {
  waitFor,
};
