import * as test from "tape";
import { Test } from "tape";
import { waitFor, TimeoutError, InvalidFunctionError } from "../lib";

test("waitFor works with simple interval", async (t: Test) => {
  let a = 0;

  // Start an interval
  const interval = setInterval(() => a++, 500);

  // Wait for 3 checks
  const result = await waitFor(async () => a >= 3 || null);
  t.equals(result, true, "result is true");

  clearInterval(interval);
  t.end();
});

test("waitFor times out with simple interval", async (t: Test) => {
  let a = 0;

  // Start an interval
  const interval = setInterval(() => a++, 500);

  try {
    // Wait for 30 checks (which should never happen)
    const result = await waitFor(async () => a >= 30 || null);
    t.fail("TimeoutError should have been thrown");
  } catch (err) {
    if (!(err instanceof TimeoutError)) {
      t.err(err, "unexpected error thrown");
      throw err;
    }

    t.pass("timeout error was thrown");
  }

  clearInterval(interval);
  t.end();
});

test("waitFor throws invalid function error", async (t: Test) => {
  try {
    // Wait for 30 checks (which should never happen)
    const result = await waitFor(null as any);
    t.fail("InvalidFunctionError should have been thrown");
  } catch (err) {
    if (!(err instanceof InvalidFunctionError)) {
      t.err(err, "InvalidFunctionError should have been thrown");
      throw err;
    }

    t.pass("InvalidFunctionError was thrown");
  }

  t.end();
});

test("waitFor throws on invalid intervalMs option", async (t: Test) => {
  try {
    // Wait for 30 checks (which should never happen)
    const result = await waitFor(async () => true, { intervalMs: -1 });
    t.fail("TypeError should have been thrown");
  } catch (err) {
    if (!(err instanceof TypeError)) {
      t.err(err, "TypeError sould have been thrown");
      throw err;
    }

    t.pass("TypeError was thrown");
  }

  t.end();
});

test("waitFor throws on invalid timeoutMs option", async (t: Test) => {
  try {
    // Wait for 30 checks (which should never happen)
    const result = await waitFor(async () => true, { timeoutMs: -1 });
    t.fail("TypeError should have been thrown");
  } catch (err) {
    if (!(err instanceof TypeError)) {
      t.err(err, "TypeError sould have been thrown");
      throw err;
    }

    t.pass("TypeError was thrown");
  }

  t.end();
});

test("waitFor works with named timeout", async (t: Test) => {
  const name = "example";
  try {
    // Wait for 30 checks (which should never happen)
    const result = await waitFor(async () => null, { timeoutMs: 0, name });
    t.fail("TimeoutError should have been thrown");
  } catch (err) {
    if (!(err instanceof TimeoutError)) {
      t.err(err, "TimeoutError sould have been thrown");
      throw err;
    }

    t.assert(
      err.message.includes(`[${name}]`),
      `error message contains configured timeout name`
    );
  }

  t.end();
});
